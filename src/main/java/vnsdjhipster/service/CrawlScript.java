package vnsdjhipster.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import vnsdjhipster.domain.OrganizationEntity;
import vnsdjhipster.repository.OrganizationEntityRepository;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.UUID;

@Service
@Transactional
public class CrawlScript {

    @Autowired
    private OrganizationEntityRepository organizationEntityRepository;
    @Autowired
    private Environment environment;

    public  ArrayList readCSV() throws IOException {
        BufferedReader csvReader = new BufferedReader(new FileReader(this.getClass().getClassLoader().
            getResource("csv/odm_org_(first_2018_vietnoys).csv").getPath()));
        ArrayList allUUID = new ArrayList();

        String row;
        int i = 0;
        while ((row = csvReader.readLine() )!= null) {
            String[] data = row.split(",");
            allUUID.add(data[0]);
            i++;
        }
        csvReader.close();
        return allUUID;
    }

    public  void insertDatabase(ArrayList input) throws IOException, URISyntaxException {
        String userKey = environment.getProperty("crunchbaseConfig.userkey");
        int count = 0;
        String content = null;
        for (Object object: input
             ) {
                String uuid = (String) object;
                String cbApiUrl = environment.getProperty("crunchbaseConfig.url")+ uuid + "?user_key="+userKey;
                InputStreamReader is;
                BufferedReader in;
                URL url = new URL(cbApiUrl);
                System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("userId", userKey);
                switch (con.getResponseCode()) {
                    case HttpURLConnection.HTTP_OK:
                        is = new InputStreamReader (con.getInputStream(), "UTF-8");
                        in = new BufferedReader (is);
                        String line = null;
                        while ((line = in.readLine()) != null) {
                            content = content + line +  "\n";
                            count++;
                            System.out.println("line " + count + " :" + line);
                            ObjectMapper mapper = new ObjectMapper();
                            mapper.configure(
                                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
                            JsonNode jsonNode = mapper.readTree(line);
                            JsonNode dataNode =  jsonNode.get("data");
                            String propertiesNodeString = dataNode.get("properties").toString();
                            OrganizationEntity organizationEntity = mapper.readValue(propertiesNodeString, OrganizationEntity.class);
                            organizationEntity.setUuid( UUID.fromString(uuid));
                            organizationEntityRepository.save(organizationEntity);
                            System.out.println("object " + organizationEntity );
                        }
                        break;
                    case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                        System.out.println(" **gateway timeout**");
                        break;
                    case HttpURLConnection.HTTP_UNAVAILABLE:
                        System.out.println("**unavailable**");
                        break;
                    default:
                        System.out.println(" **unknown response code**.");
                        break ; // abort
                }



        }



    }

}
