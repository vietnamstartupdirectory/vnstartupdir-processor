package vnsdjhipster.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "organization")
public class OrganizationEntity implements Serializable {
    private int id;
    private UUID uuid;
    private String permalink;
    private String name;
    //private String alsoKnownAs;
    private String shortDescription;
    private String description;
    private String primaryRole;
    private Boolean roleCompany;
    private Boolean roleInvestor;
    private Boolean roleGroup;
    private Boolean roleSchool;
    private Timestamp foundedOn;
    private Integer foundedOnTrustCode;
    private Timestamp closedOn;
    private Integer closedOnTrustCode;
    private Integer numEmployeesMin;
    private Integer numEmployeesMax;
    private Integer totalFundingUsd;
    private Integer totalFundingVnd;
    private String stockExchange;
    private String stockSymbol;
    private Integer numberOfInvestments;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    //private String[] permalinkAliases;
    //private String investorType;
    private String contactEmail;
    private String phoneNumber;
    private Integer rank;
    private String domain;
    private String homepageUrl;
    private String facebookUrl;
    private String twitterUrl;
    private String linkedinUrl;
    private String cityName;
    private String regionName;
    private String countryCode;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "uuid")
    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Basic
    @Column(name = "permalink")
    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    @Basic
//    @Column(name = "also_known_as")
//    public String getAlsoKnownAs() {
//        return alsoKnownAs;
//    }
//
//    public void setAlsoKnownAs(String alsoKnownAs) {
//        this.alsoKnownAs = alsoKnownAs;
//    }

    @Basic
    @Column(name = "short_description")
    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "primary_role")
    public String getPrimaryRole() {
        return primaryRole;
    }

    public void setPrimaryRole(String primaryRole) {
        this.primaryRole = primaryRole;
    }

    @Basic
    @Column(name = "role_company")
    public Boolean getRoleCompany() {
        return roleCompany;
    }

    public void setRoleCompany(Boolean roleCompany) {
        this.roleCompany = roleCompany;
    }

    @Basic
    @Column(name = "role_investor")
    public Boolean getRoleInvestor() {
        return roleInvestor;
    }

    public void setRoleInvestor(Boolean roleInvestor) {
        this.roleInvestor = roleInvestor;
    }

    @Basic
    @Column(name = "role_group")
    public Boolean getRoleGroup() {
        return roleGroup;
    }

    public void setRoleGroup(Boolean roleGroup) {
        this.roleGroup = roleGroup;
    }

    @Basic
    @Column(name = "role_school")
    public Boolean getRoleSchool() {
        return roleSchool;
    }

    public void setRoleSchool(Boolean roleSchool) {
        this.roleSchool = roleSchool;
    }

    @Basic
    @Column(name = "founded_on")
    public Timestamp getFoundedOn() {
        return foundedOn;
    }

    public void setFoundedOn(Timestamp foundedOn) {
        this.foundedOn = foundedOn;
    }

    @Basic
    @Column(name = "founded_on_trust_code")
    public Integer getFoundedOnTrustCode() {
        return foundedOnTrustCode;
    }

    public void setFoundedOnTrustCode(Integer foundedOnTrustCode) {
        this.foundedOnTrustCode = foundedOnTrustCode;
    }

    @Basic
    @Column(name = "closed_on")
    public Timestamp getClosedOn() {
        return closedOn;
    }

    public void setClosedOn(Timestamp closedOn) {
        this.closedOn = closedOn;
    }

    @Basic
    @Column(name = "closed_on_trust_code")
    public Integer getClosedOnTrustCode() {
        return closedOnTrustCode;
    }

    public void setClosedOnTrustCode(Integer closedOnTrustCode) {
        this.closedOnTrustCode = closedOnTrustCode;
    }

    @Basic
    @Column(name = "num_employees_min")
    public Integer getNumEmployeesMin() {
        return numEmployeesMin;
    }

    public void setNumEmployeesMin(Integer numEmployeesMin) {
        this.numEmployeesMin = numEmployeesMin;
    }

    @Basic
    @Column(name = "num_employees_max")
    public Integer getNumEmployeesMax() {
        return numEmployeesMax;
    }

    public void setNumEmployeesMax(Integer numEmployeesMax) {
        this.numEmployeesMax = numEmployeesMax;
    }

    @Basic
    @Column(name = "total_funding_usd")
    public Integer getTotalFundingUsd() {
        return totalFundingUsd;
    }

    public void setTotalFundingUsd(Integer totalFundingUsd) {
        this.totalFundingUsd = totalFundingUsd;
    }

    @Basic
    @Column(name = "total_funding_vnd")
    public Integer getTotalFundingVnd() {
        return totalFundingVnd;
    }

    public void setTotalFundingVnd(Integer totalFundingVnd) {
        this.totalFundingVnd = totalFundingVnd;
    }

    @Basic
    @Column(name = "stock_exchange")
    public String getStockExchange() {
        return stockExchange;
    }

    public void setStockExchange(String stockExchange) {
        this.stockExchange = stockExchange;
    }

    @Basic
    @Column(name = "stock_symbol")
    public String getStockSymbol() {
        return stockSymbol;
    }

    public void setStockSymbol(String stockSymbol) {
        this.stockSymbol = stockSymbol;
    }

    @Basic
    @Column(name = "number_of_investments")
    public Integer getNumberOfInvestments() {
        return numberOfInvestments;
    }

    public void setNumberOfInvestments(Integer numberOfInvestments) {
        this.numberOfInvestments = numberOfInvestments;
    }

    @Basic
    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Basic
    @Column(name = "updated_at")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

//    @Basic
//    @Column(name = "permalink_aliases",columnDefinition = "text[]")
//    @Type( type = "string-array" )
//    public String[] getPermalinkAliases() {
//        return permalinkAliases;
//    }
//
//    public void setPermalinkAliases(String[] permalinkAliases) {
//        this.permalinkAliases = permalinkAliases;
//    }

//    @Basic
//    @Column(name = "investor_type")
//    public String getInvestorType() {
//        return investorType;
//    }
//
//    public void setInvestorType(String investorType) {
//        this.investorType = investorType;
//    }

    @Basic
    @Column(name = "contact_email")
    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Basic
    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "rank")
    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Basic
    @Column(name = "domain")
    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Basic
    @Column(name = "homepage_url")
    public String getHomepageUrl() {
        return homepageUrl;
    }

    public void setHomepageUrl(String homepageUrl) {
        this.homepageUrl = homepageUrl;
    }

    @Basic
    @Column(name = "facebook_url")
    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    @Basic
    @Column(name = "twitter_url")
    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    @Basic
    @Column(name = "linkedin_url")
    public String getLinkedinUrl() {
        return linkedinUrl;
    }

    public void setLinkedinUrl(String linkedinUrl) {
        this.linkedinUrl = linkedinUrl;
    }

    @Basic
    @Column(name = "city_name")
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Basic
    @Column(name = "region_name")
    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Basic
    @Column(name = "country_code")
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganizationEntity that = (OrganizationEntity) o;

        if (id != that.id) return false;
        if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;
        if (permalink != null ? !permalink.equals(that.permalink) : that.permalink != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
//        if (alsoKnownAs != null ? !alsoKnownAs.equals(that.alsoKnownAs) : that.alsoKnownAs != null) return false;
        if (shortDescription != null ? !shortDescription.equals(that.shortDescription) : that.shortDescription != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (primaryRole != null ? !primaryRole.equals(that.primaryRole) : that.primaryRole != null) return false;
        if (roleCompany != null ? !roleCompany.equals(that.roleCompany) : that.roleCompany != null) return false;
        if (roleInvestor != null ? !roleInvestor.equals(that.roleInvestor) : that.roleInvestor != null) return false;
        if (roleGroup != null ? !roleGroup.equals(that.roleGroup) : that.roleGroup != null) return false;
        if (roleSchool != null ? !roleSchool.equals(that.roleSchool) : that.roleSchool != null) return false;
        if (foundedOn != null ? !foundedOn.equals(that.foundedOn) : that.foundedOn != null) return false;
        if (foundedOnTrustCode != null ? !foundedOnTrustCode.equals(that.foundedOnTrustCode) : that.foundedOnTrustCode != null)
            return false;
        if (closedOn != null ? !closedOn.equals(that.closedOn) : that.closedOn != null) return false;
        if (closedOnTrustCode != null ? !closedOnTrustCode.equals(that.closedOnTrustCode) : that.closedOnTrustCode != null)
            return false;
        if (numEmployeesMin != null ? !numEmployeesMin.equals(that.numEmployeesMin) : that.numEmployeesMin != null)
            return false;
        if (numEmployeesMax != null ? !numEmployeesMax.equals(that.numEmployeesMax) : that.numEmployeesMax != null)
            return false;
        if (totalFundingUsd != null ? !totalFundingUsd.equals(that.totalFundingUsd) : that.totalFundingUsd != null)
            return false;
        if (totalFundingVnd != null ? !totalFundingVnd.equals(that.totalFundingVnd) : that.totalFundingVnd != null)
            return false;
        if (stockExchange != null ? !stockExchange.equals(that.stockExchange) : that.stockExchange != null)
            return false;
        if (stockSymbol != null ? !stockSymbol.equals(that.stockSymbol) : that.stockSymbol != null) return false;
        if (numberOfInvestments != null ? !numberOfInvestments.equals(that.numberOfInvestments) : that.numberOfInvestments != null)
            return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
        if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
//        if (permalinkAliases != null ? !permalinkAliases.equals(that.permalinkAliases) : that.permalinkAliases != null)
//            return false;
        //if (investorType != null ? !investorType.equals(that.investorType) : that.investorType != null) return false;
        if (contactEmail != null ? !contactEmail.equals(that.contactEmail) : that.contactEmail != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (rank != null ? !rank.equals(that.rank) : that.rank != null) return false;
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (homepageUrl != null ? !homepageUrl.equals(that.homepageUrl) : that.homepageUrl != null) return false;
        if (facebookUrl != null ? !facebookUrl.equals(that.facebookUrl) : that.facebookUrl != null) return false;
        if (twitterUrl != null ? !twitterUrl.equals(that.twitterUrl) : that.twitterUrl != null) return false;
        if (linkedinUrl != null ? !linkedinUrl.equals(that.linkedinUrl) : that.linkedinUrl != null) return false;
        if (cityName != null ? !cityName.equals(that.cityName) : that.cityName != null) return false;
        if (regionName != null ? !regionName.equals(that.regionName) : that.regionName != null) return false;
        if (countryCode != null ? !countryCode.equals(that.countryCode) : that.countryCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (permalink != null ? permalink.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        //result = 31 * result + (alsoKnownAs != null ? alsoKnownAs.hashCode() : 0);
        result = 31 * result + (shortDescription != null ? shortDescription.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (primaryRole != null ? primaryRole.hashCode() : 0);
        result = 31 * result + (roleCompany != null ? roleCompany.hashCode() : 0);
        result = 31 * result + (roleInvestor != null ? roleInvestor.hashCode() : 0);
        result = 31 * result + (roleGroup != null ? roleGroup.hashCode() : 0);
        result = 31 * result + (roleSchool != null ? roleSchool.hashCode() : 0);
        result = 31 * result + (foundedOn != null ? foundedOn.hashCode() : 0);
        result = 31 * result + (foundedOnTrustCode != null ? foundedOnTrustCode.hashCode() : 0);
        result = 31 * result + (closedOn != null ? closedOn.hashCode() : 0);
        result = 31 * result + (closedOnTrustCode != null ? closedOnTrustCode.hashCode() : 0);
        result = 31 * result + (numEmployeesMin != null ? numEmployeesMin.hashCode() : 0);
        result = 31 * result + (numEmployeesMax != null ? numEmployeesMax.hashCode() : 0);
        result = 31 * result + (totalFundingUsd != null ? totalFundingUsd.hashCode() : 0);
        result = 31 * result + (totalFundingVnd != null ? totalFundingVnd.hashCode() : 0);
        result = 31 * result + (stockExchange != null ? stockExchange.hashCode() : 0);
        result = 31 * result + (stockSymbol != null ? stockSymbol.hashCode() : 0);
        result = 31 * result + (numberOfInvestments != null ? numberOfInvestments.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
//        result = 31 * result + (permalinkAliases != null ? permalinkAliases.hashCode() : 0);
  //      result = 31 * result + (investorType != null ? investorType.hashCode() : 0);
        result = 31 * result + (contactEmail != null ? contactEmail.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (rank != null ? rank.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (homepageUrl != null ? homepageUrl.hashCode() : 0);
        result = 31 * result + (facebookUrl != null ? facebookUrl.hashCode() : 0);
        result = 31 * result + (twitterUrl != null ? twitterUrl.hashCode() : 0);
        result = 31 * result + (linkedinUrl != null ? linkedinUrl.hashCode() : 0);
        result = 31 * result + (cityName != null ? cityName.hashCode() : 0);
        result = 31 * result + (regionName != null ? regionName.hashCode() : 0);
        result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
        return result;
    }
}
